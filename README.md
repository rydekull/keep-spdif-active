Adding the script to execute at boot via systemd

```
ln -s $(pwd) ~/.keep-spdif-active
mkdir -p .config/systemd/user
cp keep-spdif-active.service .config/systemd/user/keep-spdif-active.service 
systemctl --user enable keep-spdif-active
systemctl --user start keep-spdif-active
```

You can see that it is running with the help of:
`journalctl --user -f`
