#!/bin/bash
set -e

pacmd_get_default_sink_name() {
  pacmd stat | awk -F": " '/^Default sink name: /{print $2}';
}

while true
do
  # Check if there's anything playing via pulseaudio
  if [ "$(pacmd list-sink-inputs | cut -c1)" == "0" ]
  then
    # Record current volume level
    PACMD_CURRENT_VOLUME=$(pacmd list-sinks | awk '/^\s+name: /{indefault = $2 == "<'$(pacmd_get_default_sink_name)'>"} /^\s+volume: / && indefault {print $3; exit}')
    # Lower the volume all the way down
    pacmd set-sink-volume @DEFAULT_SINK@ 1 > /dev/null 2>&1
    # Since nothing is playing, we'll just send a burst of sound
    pacmd play-sample 0 @DEFAULT_SINK@ > /dev/null 2>&1
    # Return the volume level
    pacmd set-sink-volume @DEFAULT_SINK@ ${PACMD_CURRENT_VOLUME} > /dev/null 2>&1
  fi
  sleep 240
done
